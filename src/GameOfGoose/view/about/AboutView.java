package GameOfGoose.view.about;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class AboutView extends BorderPane {
    private Button btnOK;

    public AboutView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        btnOK = new Button("OK");
        btnOK.setPrefWidth(60);
    }

    private void layoutNodes() {
        final Label node = new Label("Written by The Flying Dewaele Brothers.");
        setCenter(node);
        setPadding(new Insets(10));
        BorderPane.setAlignment(btnOK, Pos.CENTER_RIGHT);
        BorderPane.setMargin(btnOK, new Insets(10, 0, 0, 0));
        setBottom(btnOK);
    }

    Button getBtnOK() {
        return btnOK;
    }
}