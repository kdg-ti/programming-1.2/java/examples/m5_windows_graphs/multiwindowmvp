package GameOfGoose.view.about;

import GameOfGoose.model.SimpleModel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AboutPresenter {
    public AboutPresenter(SimpleModel model, AboutView view) {
        view.getBtnOK().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getScene().getWindow().hide();
            }
        });
    }
}