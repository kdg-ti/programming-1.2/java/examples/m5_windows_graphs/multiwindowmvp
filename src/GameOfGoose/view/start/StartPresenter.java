package GameOfGoose.view.start;

import GameOfGoose.model.SimpleModel;
import GameOfGoose.view.game.GamePresenter;
import GameOfGoose.view.game.GameView;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.WindowEvent;

public class StartPresenter {
  private final SimpleModel model;
  private final StartView view;

  public StartPresenter(SimpleModel model, StartView view) {
    this.model = model;
    this.view = view;
    addEventHandlers();
  }

  private void addEventHandlers() {
    // add window eventhandler
    view.getScene().getWindow().setOnCloseRequest(event -> closeApplication(event));
    // add view event handlers
    view.getBtnNextScreen().setOnAction(e -> setGameView());
  }

  private void setGameView() {
    GameView gameView = new GameView();
    view.getScene().setRoot(gameView);
    GamePresenter gamePresenter = new GamePresenter(model, gameView);
    gameView.getScene().getWindow().sizeToScene();
  }

  private void closeApplication(WindowEvent event) {
    Alert alert = new Alert(Alert.AlertType.WARNING);
    alert.setHeaderText("You are about to quit the game!");
    alert.setContentText("Do you really want to leave?");
    alert.setTitle("Hark Hark!");
    alert.getButtonTypes().clear();
    ButtonType no = new ButtonType("NO");
    ButtonType yes = new ButtonType("YES");
    alert.getButtonTypes().addAll(no, yes);
    alert.showAndWait();
    if (alert.getResult() == null || alert.getResult().equals(no)) {
      event.consume();
    }
  }
}
