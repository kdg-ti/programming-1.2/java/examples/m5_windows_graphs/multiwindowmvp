package GameOfGoose.view.game;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

public class GameView extends BorderPane {
    private MenuItem miAbout;
    private MenuBar menuBar;
    private Image background;

    public GameView() {
        initialiseNodes();
        layoutNodes();
    }
    private void initialiseNodes() {
        miAbout = new MenuItem("About");
        Menu mnHelp = new Menu("Help");
        mnHelp.getItems().addAll(miAbout);
        menuBar = new MenuBar(mnHelp);;
        background = new Image("/images/goosegame.jpg");
    }
    private void layoutNodes() {
        setCenter(new ImageView(background));
        setTop(menuBar);
    }

    MenuItem getMiAbout() {
        return miAbout;
    }
}
